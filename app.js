const express = require('express');
const mongoose = require('mongoose');
const logger = require("./utils/logger");
const routes = require('./lib/routes/api');
const dataCache = require('./utils/data-cache');
const app = express();

logger.debug("Overriding 'Express' logger");
//app.use(require('morgan')({ "stream": logger.stream }));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://statdota:statdota123@ec2-54-88-169-28.compute-1.amazonaws.com:27017/statdota-dev', {
  useMongoClient: true
});
mongoose.connection.once('open', function() {
	logger.info("Successfully connected to mongoDb.");
	dataCache.scheduleAllMaintenanceMethods();
	app.use(routes);
	process.env.UV_THREADPOOL_SIZE = 128;
	app.listen(process.env.port || 8080, function(){
		logger.debug("App listening on port 8080");
	});
}).on('error', function(error){
	logger.error("Encountered some error and can't connect to mongoDb. Error thrown is : " + error);
});
