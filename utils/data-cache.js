const logger = require("./logger");
const request = require('request');
const limit = require('simple-rate-limiter');
const fs= require('fs');
const openDotaApiConfig = require('../lib/configs/open-dota-api.json');
const steamWebApiConfig = require('../lib/configs/steam-web-api.json');
const League = require("../lib/models/league.js");
const ProMatch = require("../lib/models/proMatch.js");
const Team = require("../lib/models/team.js");
const Hero = require("../lib/models/hero.js");
const Item = require("../lib/models/item.js");
const ProPlayer = require('../lib/models/proPlayer.js');

const _1minute = 1000*60;
const _1hour = 1000*60*60;
const _1day = 1000*60*60*24;

/*******************************LEAGUES***************************************/
var updateLeagues = function() {
	logger.info("Updating all leagues stored on the db.");
	var leaguesOpenDotaApiUrl = openDotaApiConfig.leagues;
	request(leaguesOpenDotaApiUrl, function(error, response, body){
		if(response && response.statusCode == 200) {
			logger.info("Successfully fetched league records from openDota.");
			var leagues = JSON.parse(body);
			League.remove({}, function(error){
				if(error){
					return logger.error('Cannot delete records from collection. Exiting.')
				} else {
					League.create(leagues).then(function(fulfilled){
						logger.info('Saved leagues data into Db successfully.');
					})
				}
			});
		} else {
			return logger.error("Couldn't fetch leagues from Open Dota API.");
		}
	});
};

/*******************************MATCHES***************************************/
// Function to update the recent 500 pro matches stored in application cache.
var updateProMatches = function(){
	logger.info("Updating all pro mathes stored on the db.");
	var count = 0;
	var proMatches = [];
	var matchId;
	var bodyJson;

	var promise = fetchProMatches(null);
	promise.then(function(resolve){
		logger.info("Received first promise result");
		bodyJson = JSON.parse(resolve);
		matchId = bodyJson[bodyJson.length - 1].match_id;
		proMatches = proMatches.concat(bodyJson);
		var promise1 = fetchProMatches(matchId);
		promise1.then(function(resolve){
			bodyJson = JSON.parse(resolve);
			logger.info("Received second promise result. Length = " + bodyJson.length);
			matchId = bodyJson[bodyJson.length - 1].match_id;
			proMatches = proMatches.concat(bodyJson);
			var promise2 = fetchProMatches(matchId);
			promise2.then(function(resolve){
				bodyJson = JSON.parse(resolve);
				logger.info("Received third promise result. Length = " + bodyJson.length);
				matchId = bodyJson[bodyJson.length - 1].match_id;
				proMatches = proMatches.concat(bodyJson);
				var promise3 = fetchProMatches(matchId);
				promise3.then(function(resolve){
					bodyJson = JSON.parse(resolve);
					logger.info("Received fourth promise result. Length = " + bodyJson.length);
					matchId = bodyJson[bodyJson.length - 1].match_id;
					proMatches = proMatches.concat(bodyJson);
					var promise4 = fetchProMatches(matchId);
					promise4.then(function(resolve){
						logger.info("Received fifth promise result");
						bodyJson = JSON.parse(resolve);
						matchId = bodyJson[bodyJson.length - 1].match_id;
						proMatches = proMatches.concat(bodyJson);
						ProMatch.remove({}, function(error){
							if (error) {
								return logger.error(error);
							} else {
								logger.info("Successfully deleted all pro matches. Now adding new matches");
								ProMatch.create(proMatches).then(function(fulfilled){
									logger.info("Created records successfully.");
								},function(rejected){
									logger.error("Cant save records. \n" + rejected);
								});
							}
						});
					}, function(rejected){
							return logger.error("Some error occured.");
					});
				}, function(rejected){
						return logger.error("Some error occured.");
				});
			}, function(rejected){
					return logger.error("Some error occured.");
			});
		}, function(rejected){
				return logger.error("Some error occured.");
		});
	}, function(rejected){
			return logger.error("Some error occured.");
	});
}

// Function to fetch the most recent 100 matches from OpenDota API.
var fetchProMatches = function(startAtMatchId) {
	return new Promise(function(resolve, reject){
		var proMatchesApiUrl = openDotaApiConfig.recent_pro_matches;
		var propertiesObject = { less_than_match_id : startAtMatchId };
		request({url : proMatchesApiUrl, qs : propertiesObject}, function(error, response, body){
			if(error){
				reject(error);
			} else if(response && response.statusCode == 200){
				resolve(body);
			} else {
				reject("Some error occured.");
			}
		});
	});
}

// Function to update live matches stored on file.
var updateLiveMatches = function () {
	var steamLiveMatchesApiUrl = steamWebApiConfig.getLiveMatches;
	var propertiesObject = { key : steamWebApiConfig.apiKeyMatches };
	request({url : steamLiveMatchesApiUrl, qs: propertiesObject}, function(error, response, body) {
		if(error) {
			fs.writeFileSync("./lib/cache/live-matches.json", {});
		} else {
			if(response && response.statusCode == 200) {
				var bodyJson = JSON.parse(body);
				var matches = bodyJson.result.games;
				var filteredMatches = [];
				var promises = [];
				matches.forEach(function(match) {
					if(match.league_tier != 2 && match.league_tier != 3) {
						logger.debug("Removing match from list as its an amateur league match.");
						var index = matches.indexOf(match);
						if(index != -1) {
							logger.debug("Splicing");
							matches = matches.splice(index, 1);
							return;
						}
						return;
					}
					if(!match.dire_team || !match.radiant_team) {
						// logger.debug("Removing match from list as both teams not registered.");
						// var index = matches.indexOf(match);
						// if(index != -1) {
						// 	logger.debug("Splicing");
						// 	matches = matches.splice(index, 1);
						// 	return;
						// }
						// return;
					}
					filteredMatches.push(match);
				});

				filteredMatches.forEach(function(filteredMatch) {
						promises.push(League.findOne({leagueid : filteredMatch.league_id}, [], {})
						.then(function(fulfilledLeagueDetails){
							logger.info('League details found in Db.');
							filteredMatch.league_details = fulfilledLeagueDetails;
						}, function(reasonRadiantTeam){
							logger.error(reasonLeagueDetails);
							filteredMatch.league_details = {};
						}));
				});
				Promise.all(promises).then(function(onFullfilled) {
					logger.info("All promises received.");
					fs.writeFileSync("./lib/cache/live-matches.json","[]");
					fs.writeFileSync("./lib/cache/live-matches.json", JSON.stringify(filteredMatches));
				},function(onRejected) {
						logger.error(onRejected);
						fs.writeFileSync("./lib/cache/live-matches.json", {});
				});

				// filteredMatches.forEach(function(filteredMatch) {
				// 	promises.push(Team.findOne({team_id : filteredMatch.radiant_team.team_id}, [], {})
				// 	.then(function(fulfilledRadiantTeam){
				// 		logger.info('Radiant team details found in Db.');
				// 		filteredMatch.radiant_team_details = fulfilledRadiantTeam;
				// 	}, function(reasonRadiantTeam){
				// 		logger.error(reasonRadiantTeam);
				// 		filteredMatch.radiant_team_details = {};
				// 	}));
				//
				// 	promises.push(Team.findOne({team_id : filteredMatch.dire_team.team_id}, [], {})
				// 	.then(function(fulfilledDireTeam){
				// 		logger.info('Dire team details found in Db.');
				// 		filteredMatch.dire_team_details = fulfilledDireTeam;
				// 	}, function(reasonDireTeam){
				// 		logger.error(reasonDireTeam);
				// 		filteredMatch.dire_team_details = {};
				// 	}));
				// });
				// Promise.all(promises).then(function(onFullfilled) {
				// 	logger.info("All promises received.");
				// 	fs.writeFileSync("./lib/cache/live-matches.json", JSON.stringify(filteredMatches));
				// },function(onRejected) {
				// 		logger.error(onRejected);
				// 		fs.writeFileSync("./lib/cache/live-matches.json", {});
				// });
			}
		}
	});
};

/********************************TEAMS****************************************/

// Method to fetch the teams from OpenDota API.
var updateTeams = function(){
	logger.info("Updating all teams stored on the db.");
	var teamsOpenDotaApiUrl = openDotaApiConfig.teams;
	var teamsSteamWebApiUrl = steamWebApiConfig.getTeamDetails;
	var teams = [];
	var teamProPlayersMapping = [];
	var promises = [];
	request(teamsOpenDotaApiUrl, function(error, response, body){
		if(response && response.statusCode == 200) {
			logger.info("Successfully fetched team records from openDota.");
			var teams = JSON.parse(body);
			var infoPromises = [];
			var logoPromises = [];
			var limitedRequest = limit(require("request")).to(2).per(1000);
			teams.forEach(function(team) {
				infoPromises.push(fetchTeamInfo(limitedRequest, team));
			});
			Promise.all(infoPromises).then(function(fulfilled) {
				logger.info("All promises complete. Now to fetch team logos.");
				teams.forEach(function(team) {
					logoPromises.push(fetchTeamLogo(limitedRequest, team));
				});
				Promise.all(logoPromises).then(function(fulfilled) {
					logger.info("All promises complete. Now to save team details into Db.");
					Team.remove({}, function(error){
						if(error){
							return logger.error('Cannot delete records from collection. Exiting.')
						} else {
							Team.create(teams).then(function(fulfilled){
								logger.info('Saved team data into Db successfully.');
							}, function(rejected) {
								logger.error('Cannot save team details into Db. Error: ' + rejected);
							});
						}
					});
				}, function(rejected) {
					logger.error('One of the queries encountered an error.' + rejected);
				});
			}, function(rejected) {
				logger.error('One of the queries encountered an error.' + rejected);
			});

		} else {
			return logger.error("Couldn't fetch teams from Open Dota API.");
		}
	});
};

//Method to fetch team info from steam API.
var fetchTeamInfo = function(limitedRequest, team) {
	var promise = new Promise(function(resolve, reject) {
		var teamsSteamWebApiUrl = steamWebApiConfig.getTeamDetails;
		var propertiesObject = { key : steamWebApiConfig.apiKeyTeams, team_id : team.team_id };
		limitedRequest({ url : teamsSteamWebApiUrl, qs : propertiesObject }, function(error, response, body) {
			if(error) {
				var emptyObject = {};
				logger.error("No response from steam api about team with team id: " + team.team_id);
				resolve(emptyObject);
			}
			if(response && response.statusCode == 200) {
				logger.info("Successfully fetched team details from Steam for team Id : " + team.team_id);
				var bodyJson = JSON.parse(body);
				resolve(bodyJson);
			}
		});
	});
	promise.then(function(bodyJson) {
		team.time_created= bodyJson.teams[0].time_created,
		team.pro= bodyJson.teams[0].pro,
		team.locked= bodyJson.teams[0].locked,
		team.ugc_logo= bodyJson.teams[0].ugc_logo,
		team.ugc_base_logo= bodyJson.teams[0].ugc_base_logo,
		team.ugc_banner_logo= bodyJson.teams[0].ugc_banner_logo,
		team.ugc_sponsor_logo= bodyJson.teams[0].ugc_sponsor_logo,
		team.country_code= bodyJson.teams[0].country_code,
		team.url= bodyJson.teams[0].url
	}, function(reject) {
		logger.error("Error received : " + reject);
	});
	return promise;
}


//Method to fetch team logo from steam API.
var fetchTeamLogo = function(limitedRequest, team) {
	var promise = new Promise(function(resolve, reject) {
		var resolveUrlSteamWebApiUrl = steamWebApiConfig.resolveUrl;
		var propertiesObject = { key : steamWebApiConfig.apiKeyTeams, ugcid : team.ugc_logo, appId: 570 };
		limitedRequest({ url : resolveUrlSteamWebApiUrl, qs : propertiesObject }, function(error, response, body) {
			if(error) {
				logger.error("No response from steam api about team with team id: " + team.team_id);
				resolve("");
			}
			if(response && response.statusCode == 200) {
				logger.info("Successfully fetched team logo details from Steam for team Id : " + team.team_id);
				var bodyJson = JSON.parse(body);
				resolve(bodyJson);
			}
		});
	});
	promise.then(function(resolved) {
		team.logo_url = resolved.data.url;
	}, function(reject) {
		logger.error("Error received : " + reject);
	});
	return promise;
}


/****************************PRO PLAYERS**************************************/

//Method to update the pro players.
var updateProPlayers = function(){
	logger.info("Updating all pro players stored on the db.");
	var proPlayersApiUrl = openDotaApiConfig.pro_players;
	request(proPlayersApiUrl, function(error, response, body){
		if(response && response.statusCode ==200){
			logger.info("Successfully fetched pro players records from openDota.");
			var bodyJson = JSON.parse(body);
			ProPlayer.remove({}, function(error){
				if(error){
					return logger.error('Cannot delete records from collection. Exiting.')
				} else {
					ProPlayer.create(bodyJson).then(function(fulfilled){
						logger.info("Created pro player records successfully.");
					},function(rejected){
						logger.info("Cant save pro player records. \n" + rejected);
					});
				}
			});
		} else {
			logger.error("Cant fetch pro player records from openDota.");
		}
	});
};


/*******************************HEROES****************************************/
// Function to update hero details stored in application cache.
var updateHeroDetails = function(){
	logger.info("Updating all heroes stored on the db.");
	var openDotaHeroDetailsUrl = openDotaApiConfig.heroes;
	request(openDotaHeroDetailsUrl, function(error, response, body) {
		if(error) {
			logger.info("Error");
			return logger.error(error);
		} else if(response && response.statusCode == 200) {
			var bodyJson = JSON.parse(body);
			Hero.remove({}, function(error){
				if(error){
					return logger.error('Cannot delete records from collection. Exiting.')
				} else {
					Hero.create(bodyJson).then(function(fulfilled){
						logger.info("Created records successfully.");
					},function(rejected){
						return logger.error("Cant save records. \n" + rejected);
					});
				}
			});
		} else {
			return logger.error("Cant save records.");
		}
	});
};

/*******************************ITEMS****************************************/
var updateItemDetails = function(){
	logger.info("Updating all items stored on the db.");
	var steamWbApiItemDetailsUrl = steamWebApiConfig.getItemDetails;
	var propertiesObject = { key : steamWebApiConfig.apiKeyDefault}
	request({ url : steamWbApiItemDetailsUrl, qs : propertiesObject }, function(error, response, body) {
		if(error) {
			logger.info("Error");
			return logger.error(error);
		} else if(response && response.statusCode == 200) {
			var bodyJson = JSON.parse(body);
			Item.remove({}, function(error){
				if(error){
					return logger.error('Cannot delete records from collection. Exiting.')
				} else {
					Item.create(bodyJson.result.items).then(function(fulfilled){
						logger.info("Created records successfully.");
					},function(rejected){
						return logger.error("Cant save records. \n" + rejected);
					});
				}
			});
		} else {
			return logger.error("Cant save records.");
		}
	});
};



var scheduleAllMaintenanceMethods = function(){
	logger.info("Setting intervals on all maintanance methods.");
	updateLeagues();
	updateProMatches();
	updateLiveMatches();
 	updateTeams();
	updateProPlayers();
	updateHeroDetails();
	updateItemDetails();
	setInterval(updateLeagues, _1day);
	setInterval(updateProMatches, _1minute * 20);
	setInterval(updateLiveMatches, _1minute);
	setInterval(updateTeams, 3 * _1day);
	setInterval(updateHeroDetails, 3 * _1day);
	setInterval(updateItemDetails, 3 * _1day);
	setInterval(updateProPlayers, 3 * _1day);
};

module.exports.scheduleAllMaintenanceMethods = scheduleAllMaintenanceMethods;
