const env = process.env.NODE_ENV || 'development';
var winston = require('winston');
winston.emitErrs = true;

var logger = new winston.Logger({
    transports: [
				new (require('winston-daily-rotate-file'))({
				      filename: './logs/-logs.log',
				      datePattern: 'yyyy-MM-dd',
				      prepend: true,
	            handleExceptions: true,
	            json: true,
							colorize: true,
			        maxsize      : 102400,
			        maxFiles     : 30,
				      level: env === 'development' ? 'verbose' : 'info'
				    }),

        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true,

        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
