const steam = require('steam');
const fs = require("fs");
const crypto = require("crypto");
const steamClient = new steam.SteamClient();
const dota2 = require('dota2');
const Dota2 = new dota2.Dota2Client(steamClient, true, false);
const steamUser = new steam.SteamUser(steamClient);
const steamFriends = new steam.SteamFriends(steamClient);

var isConnectedToGameCoordinator = false;

global.config = require("../config");

var onSteamLogOn = function onSteamLogOn(logonResp) {
        if (logonResp.eresult == steam.EResult.OK) {
            steamFriends.setPersonaState(steam.EPersonaState.Busy); // to display your steamClient's status as "Online"
            steamFriends.setPersonaName(global.config.steam_name); // to change its nickname
            console.log("Logged on.");
            Dota2.launch();
            Dota2.on("ready", function() {
                console.log("Node-dota2 ready.");
								isConnectedToGameCoordinator = true;
								module.exports.isConnectedToGameCoordinator = true;
            });
            Dota2.on("unready", function onUnready() {
                console.log("Node-dota2 unready.");
								isConnectedToGameCoordinator = false;
								module.exports.isConnectedToGameCoordinator = false;
            });
            Dota2.on("chatMessage", function(channel, personaName, message) {
                // console.log([channel, personaName, message].join(", "));
            });
            Dota2.on("guildInvite", function(guildId, guildName, inviter) {
                // Dota2.setGuildAccountRole(guildId, 75028261, 3);
            });
            Dota2.on("unhandled", function(kMsg) {
                console.log("UNHANDLED MESSAGE " + dota2._getMessageName(kMsg));
            });
            // setTimeout(function(){ Dota2.exit(); }, 5000);
        }
    },
    onSteamServers = function onSteamServers(servers) {
        console.log("Received servers.");
        fs.writeFile('servers', JSON.stringify(servers), (err)=>{
            if (err) {if (this.debug) console.log("Error writing ");}
            else {if (this.debug) console.log("");}
        });
    },
    onSteamLogOff = function onSteamLogOff(eresult) {
        console.log("Logged off from Steam.");
				module.exports.isConnectedToGameCoordinator = false;
    },
    onSteamError = function onSteamError(error) {
        console.log("Connection closed by server: "+error);
				module.exports.isConnectedToGameCoordinator = false;
    };

steamUser.on('updateMachineAuth', function(sentry, callback) {
    var hashedSentry = crypto.createHash('sha1').update(sentry.bytes).digest();
    fs.writeFileSync('sentry', hashedSentry)
    console.log("sentryfile saved");
    callback({
        sha_file: hashedSentry
    });
});


// Login, only passing authCode if it exists
var logOnDetails = {
    "account_name": global.config.steam_user,
    "password": global.config.steam_pass,
};
if (global.config.steam_guard_code) logOnDetails.auth_code = global.config.steam_guard_code;
if (global.config.two_factor_code) logOnDetails.two_factor_code = global.config.two_factor_code;

try {
    var sentry = fs.readFileSync('sentry');
    if (sentry.length) logOnDetails.sha_sentryfile = sentry;
} catch (beef) {
    console.log("Cannae load the sentry. " + beef);
}

steamClient.on('connected', function() {
    steamUser.logOn(logOnDetails);
});
steamClient.on('logOnResponse', onSteamLogOn);
steamClient.on('loggedOff', onSteamLogOff);
steamClient.on('error', onSteamError);
steamClient.on('servers', onSteamServers);


module.exports = {"isConnectedToGameCoordinator" : isConnectedToGameCoordinator,  "steamClient" : steamClient, "Dota2" : Dota2};
