const fs= require('fs');
const express = require('express');
const router = express.Router();
const url = require('url');
const request = require('request');
const steamConnection = require('../../utils/steam-connection.js');
const openDotaApiConfig = require('../configs/open-dota-api.json');
const steamWebApiConfig = require('../configs/steam-web-api.json');
const generalConstants = require('../configs/general-constants.json');
const logger = require("../../utils/logger");
const League = require("../models/league.js");
const ProMatch = require("../models/proMatch.js");
const Team = require("../models/team.js");
const Hero = require("../models/hero.js");
const Item = require("../models/item.js");
const ProPlayer = require('../models/proPlayer.js');


var baseRequest = request.defaults({
	pool: {
		maxSockets : 50
	}
	,
	//agent: false,
	jar: true,
	json: true,
	//timeout: 10000,
	gzip: true,
	headers: {
		'Content-Type': 'application/json'
	}
});


steamConnection.steamClient.connect();

/********************************* METADATA ***********************************/
router.get('/metadata/update', function(req, res) {
	logger.info("Request received: " + req.url);
	var metadata = {};
	var promises = [];
	promises.push(Hero.find({}, [], {}).then(function(fulfilled){
		logger.info("Fetched hero records successfully.");
		metadata.hero_details = fulfilled;
	},function(rejected){
		logger.error("Couldn't fetch hero records successfully.\n" + rejected);
	}));

	promises.push(Item.find({}, [], {}).then(function(fulfilled){
		logger.info("Fetched hero records successfully.");
		metadata.item_details = fulfilled;
	},function(rejected){
		logger.error("Couldn't fetch hero records successfully.\n" + rejected);
	}));

	Promise.all(promises).then(function(fulfilled){
		res.status(200).send(metadata);
	}, function(rejected){
		res.status(500).send("Failure");
	});
});

/********************************* PROFILE ***********************************/

//Get all default data for a certain player
router.get('/players/:steamId', function(req, res){
	logger.info("Request received: " + req.url);
	var player = {};
	var placeholder = {};
	var openDotaPlayerUrl = openDotaApiConfig.players;
	var openDotaPlayerTotalsUrl = openDotaApiConfig.players_totals;
	var openDotaPlayerWinLossUrl = openDotaApiConfig.players_win_loss;
	openDotaPlayerUrl = openDotaPlayerUrl.replace("{account_id}", req.params.steamId);
	openDotaPlayerTotalsUrl = openDotaPlayerTotalsUrl.replace("{account_id}", req.params.steamId);
	openDotaPlayerWinLossUrl = openDotaPlayerWinLossUrl.replace("{account_id}", req.params.steamId);
	var playerBasicInfoPromise = new Promise(function(resolve, reject) {
		if(steamConnection.isConnectedToGameCoordinator === true)
		{
			logger.info("Is connected to game coordinator");
			steamConnection.Dota2.requestPlayerStats(req.params.steamId, function(errorCode, responseMessage){
				if(errorCode)
				{
					resolve(placeholder);
				} else {
					resolve(responseMessage);
				}
			});
		} else {
			resolve(placeholder);
		}
	});

	playerBasicInfoPromise.then(function(response) {
		logger.info("Successfully fetched player basic info from steam GC. \n" + JSON.stringify(response));
		player.basicInfo = response;
	}, function(error) {
		logger.error("Failed to fetch player basic info from steam GC." + error);
		player.basicInfo = {};
	});

	var playerInfoPromise = new Promise(function(resolve, reject) {
		request(openDotaPlayerUrl, function (error, response, body) {
			if(error != null)
			{
				resolve(placeholder);
			} else {
				resolve(body);
			}
		});
	});

	playerInfoPromise.then(function(response) {
		logger.info("Successfully fetched player info from openDota api.");
		player.playerInfo = JSON.parse(response);
	}, function(error) {
		logger.error("Failed to fetch player info from openDota api." + error);
		player.playerInfo = {};
	});

	var playerTotalPromise = new Promise(function(resolve, reject) {
		request(openDotaPlayerTotalsUrl, function (error, response, body) {
			if(error != null)
			{
				resolve(placeholder);
			} else {
				resolve(body);
			}
		});
	});

	playerTotalPromise.then(function(response) {
		logger.info("Successfully fetched player totals info from openDota api.");
		player.totals = JSON.parse(response);
	}, function(error) {
		logger.error("Failed to fetch player totals info from openDota api." + error);
		player.totals = {};
	});

	var playerWinLossPromise = new Promise(function(resolve, reject) {
		request(openDotaPlayerWinLossUrl, function (error, response, body) {
			if(error != null)
			{
				resolve(placeholder);
			} else {
				resolve(body);
			}
		});
	});

	playerWinLossPromise.then(function(response) {
		logger.info("Successfully fetched player win loss info from openDota api.");
		player.winLoss = JSON.parse(response);
	}, function(error) {
		logger.error("Failed to fetch player win loss info from openDota api." + error);
		player.winLoss = {};
	});

	Promise.all([playerBasicInfoPromise, playerInfoPromise, playerTotalPromise, playerWinLossPromise]).then(function(response) {
		res.send(player);
	}, function(error) {
		logger.error("Failed atleast one promise!" + error);
		res.status(500).send("Failure");
	});
});

// Get matches details for player.
router.get('/player/:steamId/matches', function(req,res) {
	logger.info("Request received: " + req.url);
	var findMatchesUrl = openDotaApiConfig.matches;
	findMatchesUrl = findMatchesUrl.replace("{account_id}", req.params.steamId);
	var propertiesObject = { limit: req.query.limit, offset: req.query.offset, project: ["kills", "deaths", "assists", "gold_per_min", "xp_per_min", "hero_id", "lane_role"] };
	request({url:findMatchesUrl, qs:propertiesObject}, function(error, response, body){
		if(error)
		{
			logger.error("Some error in fetches recent personal player matches from open Dota API.");
			res.status(500).send("Failure.");
		} else if(response && response.statusCode == 200) {
			body = JSON.parse(body);
			var promises = [];
			body.forEach(function(match) {
				promises.push(
					Hero.findOne({id : match.hero_id},[],{}).then(function(fulfilled) {
						match.hero_details = fulfilled;
					}, function(rejected) {
						match.hero_details = {};
					})
				);
				match.game_details = generalConstants["game_modes"][match.game_mode];
			});
			Promise.all(promises).then(function(fulfilled) {
				res.send(body);
			}, function(rejected) {
				logger.error("Some error in fetching hero info from internal cache.");
				res.status(500).send("Failure.");
			});
		} else {
			logger.error("Some error in fetches recent personal player matches from open Dota API.");
			res.status(500).send("Failure.");
		}
	});
});

//Get hero related data.
router.get('/player/:steamId/heroes', function(req, res) {
		logger.info("Request received: " + req.url);
		var findHeroDetails = openDotaApiConfig.player_heroes;
		findHeroDetails = findHeroDetails.replace("{account_id}", req.params.steamId);
		request(findHeroDetails, function(error, response, body){
			if(error){
				logger.error("Some error in fetches personal player hero info from open Dota API.");
				res.status(500).send("Failure.");
			} else if(response && response.statusCode == 200) {
				body = JSON.parse(body);
				var promises = [];
				body.forEach(function(hero) {
					promises.push(
						Hero.findOne({id : hero.hero_id},[],{}).then(function(fulfilled) {
							hero.hero_details = fulfilled;
						}, function(rejected) {
							hero.hero_details = {};
						})
					);
				});
				Promise.all(promises).then(function(fulfilled) {
					res.send(body);
				}, function(rejected) {
					logger.error("Some error in fetching hero info from internal cache.");
					res.status(500).send("Failure.");
				});
			} else {
				logger.error("Some error in fetches personal player hero info from open Dota API.");
				res.status(500).send("Failure.");
			}
		});
});

//Get friends related data.
router.get('/player/:steamId/peers', function(req,res){
	logger.info("Request received: " + req.url);
	var openDotaPeersUrl = openDotaApiConfig.peers;
	openDotaPeersUrl = openDotaPeersUrl.replace("{account_id}", req.params.steamId);
	request(openDotaPeersUrl, function (error, response, body) {
		if(error){
				res.status(500).send("Failure.");
		}
		else if(response && response.statusCode == 200)
		{
			res.send(JSON.parse(body));
		} else {
			res.status(500).send("Failure.");
		}
	});
});

router.get('/players/parse/:steamId', function(req, res) {
	logger.info("Request received: " + req.url);
});


//Get details about a certain match.
router.get('/matches/:matchId', function(req,res) {
	logger.info("Request received: " + req.url);
	var findMatchDetailsUrl = steamWebApiConfig.getMatchDetails;
	var propertiesObject = { key: steamWebApiConfig.apiKey, match_id: req.params.matchId };
	request({url:findMatchDetailsUrl, qs:propertiesObject}, function(err, response, body) {
		if(err) {
			console.log(err);
			res.status(500).send("Failure.");
		}
		if(response && response.statusCode == 200)
		{
			var promisesArray = [];
			var playersArray = [];
			body = JSON.parse(body);
			var heroDetailsJsonArray = JSON.parse(heroDetails);
			for(i = 0; i < body.result.players.length; i++)
			{
				var openDotaPlayerUrl = openDotaApiConfig.players;
				openDotaPlayerUrl = openDotaPlayerUrl.replace("{account_id}", body.result.players[i].account_id);
				for(j=0; j< heroDetailsJsonArray.length; j++){
					if(heroDetailsJsonArray[j].id === body.result.players[i].hero_id){
						body.result.players[i].heroDetails = heroDetailsJsonArray[j];
					}
				}
				promisesArray[i] = new Promise(function(resolve, reject) {
					request(openDotaPlayerUrl, function (error, response, body1) {
						if(error)
						{
							reject(error);
						} else {
							resolve(body1);
						}
					});
				});
				promisesArray[i].then(function(response) {
					console.log("Successfully fetched account data.", response);
					playersArray.push(JSON.parse(response));
				}, function(error) {
					console.error("Failed!", error);
					playersArray.push(null);
				});
			}

			Promise.all(promisesArray).then(function(response) {
				console.log("All user data fetched successfully");
				body.result.playersArray = playersArray;
				res.send(body);
			}, function(error) {
				console.error("Failed atleast one!", error);
				body.result.playersArray = playersArray;
				res.send(body);
			});
		}
	});
});


/********************************* LEAGUES ***********************************/

//Endpoint to return all leagues.
router.get('/leagues', function(req, res) {
	console.log("Request received: " + req.url);
	var metadata = {};
	var promises = [];
	League.find({}, [], {}).then(function(fulfilled){
		console.log("Fetched league records successfully.");
		res.status(200).send(fulfilled);
	},function(rejected){
		console.error("Couldn't fetch hero records successfully.\n" + rejected);
		res.status(500).send("Failure");
	});
});

/********************************* MATCHES ***********************************/
// Get all recent league matches.
router.get('/leagues/matches', function(req,res){
	logger.info("Request received: " + req.url);
	ProMatch.find({}, ['match_id',
	'duration',
	'start_time',
	'radiant_team_id',
	'radiant_name',
	'dire_team_id',
	'dire_name',
	'leagueid',
	'league_name',
	'series_id',
	'series_type',
	'radiant_win'], {
    skip:parseInt(req.query.offset), // Starting Row
    limit:parseInt(req.query.limit), // Ending Row
    sort:{
			start_time : -1
    }
	}).then(function(fulfilled){
		var matches = [];
		fulfilled.forEach(function(match){
			matches.push(match.toJSON());
		});
		var promises = [];
		matches.forEach(function(match){
			promises.push(Team.findOne({team_id : match.radiant_team_id}, [], {})
			.then(function(fulfilledRadiantTeam){
				logger.info('Radiant team details found in Db.');
				match.radiant_team_details = fulfilledRadiantTeam;
				console.log(match);
			}, function(reasonRadiantTeam){
				console.error(reasonRadiantTeam);
				match.radiant_team_details = {};
			}));

			promises.push(Team.findOne({team_id : match.dire_team_id}, [], {})
			.then(function(fulfilledDireTeam){
				logger.info('Dire team details found in Db');
				match.dire_team_details = fulfilledDireTeam;
			}, function(reasonDireTeam){
				console.error(reasonDireTeam);
				match.dire_team_details = {};
			}));
			if(match.series_type === 0)
			{
				match.game_number = 1;
			} else {
				promises.push(ProMatch.find({series_id : match.series_id},
					['match_id',
					'start_time',
					'series_id',
					'series_type',
					'radiant_win'],
					{}
				).then(function(fulfilled){
					if(fulfilled.length > 0) {
						var numberOfCompletedGames = 0;
						fulfilled.forEach(function(completedGame){
							if(completedGame.start_time < match.start_time) {
								numberOfCompletedGames++;
							}
						});
						match.game_number = numberOfCompletedGames + 1;
					} else {
						match.game_number = 1;
					}
				}, function(reason){
					console.error(reason);
					match.game_number = 1;
				}));
			}
		});
		Promise.all(promises).then(function(onFullfilled) {
			logger.info("All promises received.");
			res.send(matches);
		},function(onRejected) {
				logger.info(onRejected);
		});
	}, function(reason){
		console.error(reason);
		res.status(500).send("Failure.");
	});
});

// Get all live league matches.
router.get('/leagues/liveMatches', function(req,res){
	logger.info("Request received: " + req.url);
	// var readStream = fs.createReadStream('./lib/cache/live-matches.json','utf-8');
	var buf = fs.readFileSync('./lib/cache/live-matches.json', "utf8");
	res.send(buf);
});

// Get match details for specific live league match.
router.get('/leagues/liveMatch/:matchId', function(req,res){
	logger.info("Request received: " + req.url);
	var result;
	var buf = fs.readFileSync('./lib/cache/live-matches.json', "utf8");
	var bodyJson = JSON.parse(buf);
	bodyJson.forEach(function(element, index, array) {
		if(element.match_id == req.params.matchId) {
			result = element;
		}
	});
	if(!result) {
		res.status(500).send("Failure.");
	} else {
		res.send(result);
	}
});

//Get match details for a specific match.
router.get('/match/:matchId', function(req, res) {
	logger.info("Request received: " + req.url);
	var openDotaMatchDetailsUrl = openDotaApiConfig.match_details;
	openDotaMatchDetailsUrl = openDotaMatchDetailsUrl.replace("{match_id}", req.params.matchId);
	baseRequest(openDotaMatchDetailsUrl, function(error,response,body){
		logger.info("Recieved match details from openDota.");
		if(error) {
			logger.error(error);
			res.status(500).send("Failure.");
		} else if(response && response.statusCode == 200) {
			res.send(body);
		} else {
			res.status(500).send("Failure.");
		}
	});
});

router.get('/request/:matchId', function(req, res) {
	logger.info("Request received: " + req.url);
	var openDotaMatchDetailsUrl = openDotaApiConfig.request_parse;
	openDotaMatchDetailsUrl = openDotaMatchDetailsUrl.replace("{match_id}", req.params.matchId);
	request.post(openDotaMatchDetailsUrl, function(error,response,body){
		if(error) {
			res.status(500).send("Failure.");
		} else if(response && response.statusCode == 200) {
			res.send(JSON.parse(body));
		} else {
			res.status(500).send("Failure.");
		}
	});
});

/********************************* TEAMS ***********************************/

//Get teams related data.
router.get('/teams', function(req,res){
	console.log("Request received: " + req.url);
	Team.find({rating: { $ne: null }}, [], {
    skip:parseInt(req.query.offset), // Starting Row
    limit:parseInt(req.query.count), // Ending Row
    sort:{
        rating: -1
    }
	}).then(function(fulfilled){
		res.send(fulfilled);
	}, function(reason){
		console.error(reason);
		res.status(500).send("Failure.");
	});
});

/******************************PRO PLAYERS***********************************/
//Get pro players related data.
router.get('/proPlayers', function(req,res){
	logger.info("Request received: " + req.url);
	ProPlayer.find({team_id: { $ne: 0 } , is_pro: { $ne: false }}, [], {
		skip:parseInt(req.query.offset), // Starting Row
		limit:parseInt(req.query.limit), // Ending Row
		sort:{
				name: 1
		}
	}).then(function(fulfilled){
		logger.info("Successfully fetched pro player records from Db.");
		res.send(fulfilled);
	}, function(reason){
		logger.error(reason);
		res.status(500).send("Failure.");
	});
});


module.exports = router;
