const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamProPlayerMappingSchema = new Schema({
	"account_id":Number,
	"time_joined":Number,
	"admin":Boolean,
	"sub":Boolean
});

const TeamProPlayerMappingModel = mongoose.model('TeamProPlayerMapping', TeamProPlayerMappingSchema);

module.exports = TeamProPlayerMappingModel;
