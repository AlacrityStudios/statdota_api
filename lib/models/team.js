const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TeamSchema = new Schema({
	"team_id": { type: Number, default: 0 },
	"rating": { type: Number, default: 0 },
	"wins": { type: Number, default: 0 },
	"losses": { type: Number, default: 0 },
	"name": { type: String, default: "" },
	"tag": { type: String, default: "" },

	"time_created": { type: Number, default: 0 },
	"pro": { type: Boolean, default: true },
	"locked": { type: Boolean, default: true },
	"ugc_logo": { type: String, default: "" },
	"ugc_base_logo": { type: String, default: "" },
	"ugc_banner_logo": { type: String, default: "" },
	"ugc_sponsor_logo": { type: String, default: "" },
	"country_code": { type: String, default: "" },
	"url": { type: String, default: "" },
	"logo_url": { type: String, default: "" }
});

const TeamModel = mongoose.model('Team', TeamSchema);

module.exports = TeamModel;
