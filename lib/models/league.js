const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LeagueSchema = new Schema({
	"leagueid": { type: Number, default: 0 },
	"ticket": { type: String, default: "" },
	"banner": { type: String, default: "" },
	"tier": { type: String, default: "" },
	"name": { type: String, default: "" }
});

const LeagueModel = mongoose.model('League', LeagueSchema);

module.exports = LeagueModel;
