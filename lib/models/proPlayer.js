const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProPlayerSchema = new Schema({
	"account_id":{ type: Number, default: 0 },
	"steamid": { type: String, default: "" },
	"avatar": { type: String, default: "" },
	"avatarmedium": { type: String, default: "" },
	"avatarfull": { type: String, default: "" },
	"profileurl": { type: String, default: "" },
	"personaname": { type: String, default: "" },
	"last_login": { type: String, default: "" },
	"full_history_time":{ type: String, default: 0 },
	"cheese":{ type: Number, default: 0 },
	"fh_unavailable": { type: Boolean, default: 0 },
	"loccountrycode": { type: String, default: "" },
	"name": { type: String, default: "" },
	"country_code": { type: String, default: "" },
	"fantasy_role":{ type: Number, default: 0 },
	"team_id":{ type: Number, default: 0 },
	"team_name": { type: String, default: "" },
	"team_tag": { type: String, default: "" },
	"is_locked": { type: Boolean, default: 0 },
	"is_pro": { type: Boolean, default: 0 },
	"locked_until":{ type: Number, default: 0 }
});

const ProPlayerModel = mongoose.model('ProPlayer', ProPlayerSchema);

module.exports = ProPlayerModel;
