const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HeroSchema = new Schema({
	"id": { type: Number, default: 0 },
	"name": { type: String, default: "" },
	"localized_name": { type: String, default: "" },
	"primary_attr": { type: String, default: "" },
	"attack_type": { type: String, default: "" }
});

const HeroModel = mongoose.model('Hero', HeroSchema);

module.exports = HeroModel;
