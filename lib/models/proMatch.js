const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProMatchSchema = new Schema(
	{
			"match_id":Number,
			"duration":Number,
			"start_time":Number,
			"radiant_team_id":Number,
			"radiant_name":String,
			"dire_team_id":Number,
			"dire_name":String,
			"leagueid":Number,
			"league_name":String,
			"series_id":Number,
			"series_type":{ type: Number },
			"radiant_win":Boolean
	}
);

ProMatchSchema.pre('save', function(next) {
    if (this.series_type === null) {
      this.series_type = 0;
    }
    next();
});

const ProMatchModel = mongoose.model('ProMatch', ProMatchSchema);

module.exports = ProMatchModel;
