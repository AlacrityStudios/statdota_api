const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
	"id": { type: Number, default: 0 },
	"name": { type: String, default: "" },
	"cost": { type: Number, default: 0 },
	"secret_shop": { type: Number, default: 0 },
	"side_shop": { type: Number, default: 0 },
	"recipe": { type: Number, default: 0 }
});

const ItemModel = mongoose.model('Item', ItemSchema);

module.exports = ItemModel;
